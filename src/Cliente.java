public class Cliente {

	public static void main(String[] args) {
				
		 Estudiante[] estudiante= new Estudiante[]{
	                new Estudiante(15,"Rodrigo", 20),
	                new Estudiante(12,"Angela",11),
	                new Estudiante(22,"Miguel",18),
	                new Estudiante(10,"Rodanta",13)  
	        };
	       
		 	Sistema c =  new Sistema();
		 	c.registar("Notas", new Notas());
		 	c.registar("Codigo", new Codigo());
		 	c.registar("Nombre", new Nombre());
		 	
		 	//c.ejecutar(estudiante, "Notas");
		 	//c.ejecutar(estudiante, "Codigo");
		 	c.ejecutar(estudiante, "Nombre");
		 	
		 	 System.out.println("Codigo"+" " +" Nombre"+" "+" "+"Nota");
		 	 
		 	 for(int i =0;i<estudiante.length;i++){
		            System.out.println(estudiante[i].codigo+ "  -  "+estudiante[i].nombre+"  -  "+estudiante[i].nota);
		 	 }
	   

	}
}