import java.util.HashMap;
import java.util.Map;


public class Sistema {

	
	Map<String, Ordenamiento> tipo_ordenamiento;

		public Sistema() {
			tipo_ordenamiento = new HashMap<String, Ordenamiento>();
		}
		
		
		public void registar(String operador, Ordenamiento operacion) {
			tipo_ordenamiento.put(operador, operacion);
		}

		
			
		public void ejecutar(Estudiante[] estudiante , String operador) {

				Ordenamiento operacion = tipo_ordenamiento.get(operador);

				if (operacion == null) {
					System.out.println("No existe la operacion");
					
				}

					operacion.burbuja(estudiante);

				}




		
	
}
